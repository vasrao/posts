import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Body, View } from 'native-base';

export default class HomeScreen extends Component {

  constructor(){

    super();
        this.state = {
            posts:[]
         }
         this.getData();
  }

  getData = () => {
  
        fetch('https://jsonplaceholder.typicode.com/posts' , {        
          method: 'GET',                      
          headers: {   
            'Accept': 'application/json',       
            'Content-Type': 'application/json',    
          }      
        })   
          .then((response) => response.json())    
          .then((responseJson) => {
            console.log('responseJson' + JSON.stringify(responseJson));    
            this.setState({'posts' : responseJson})    
            console.log(this.state.posts); 
          })  
                          
          .catch((error) => {
            console.error(error);          
          });      
          //console.log(this.state.posts);  
  } 

  render() {
    cards = this.state.posts.map((post) => {
      return (
      <Card>
         <CardItem header>
              <Text>title</Text>
               <Text>{post.title}</Text>
         </CardItem>
         <CardItem>
           <Body>
            <Text>body</Text>
             <Text>
             {post.body}
             </Text>
           </Body>
         </CardItem>
     </Card>
    )});

    return (
      <Container>
        <Header>
        <Text>POSTS </Text>
        </Header>
        <Content>
           {cards}
        </Content>
      </Container>
    );
  }
}
