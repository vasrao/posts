import { StackNavigator } from "react-navigation";
import HomeScreen from "./HomeScreen";

const Home = StackNavigator(
  {
    Home : {screen : HomeScreen}
  }
 
);    

export default Home;