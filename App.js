import React, {Component} from 'react';
import { StyleSheet, Text, View, AppRegistry} from 'react-native';
import HomeScreen from './src/home/HomeScreen';
import {Expo} from 'expo';


export default class AwesomeApp extends Component {

  render() {

  return < HomeScreen/>;

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

AppRegistry.registerComponent('Demansol_Tech', () => AwesomeApp);

